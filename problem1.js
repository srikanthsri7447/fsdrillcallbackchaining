const fs = require('fs')
const path = require("path");



//Create Directory 

function createDir(dirName, callback) {
    fs.mkdir(path.join(__dirname, dirName), (err) => {
        if (err) {
            console.log(err.message)
        }
        else {
            console.log('Directory Created')
            callback()
        }
    })
}

//Create File in Directory

function crtFile(filePath, callback) {
    fs.writeFile(path.join(filePath, ''), 'Hello World!', 'utf8', (err) => {
        if (err) {
            console.log(err.message)
        }
        else {
            console.log('File Created')
            callback()

        }
    })
}

// Delete file in Directory

function deleteFiles(filePath, callBack) {
    fs.unlink(path.join(filePath, ''), (err) => {
        if (err) {
            console.log(err.message)
        }
        else {
            console.log('Json File Deleted')
            callBack()
        }
    })
};




module.exports = { createDir, crtFile, deleteFiles };