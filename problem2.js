const fs = require('fs')
const path = require('path')


function readFile(filePath, callback) {
    fs.readFile(path.join(filePath, ''), 'utf-8', (err, data) => {
        if (err) {
            console.log(err.message)
        }
        else {
            console.log(`${filePath} Reading Sucessfully `)
            callback(data)
        }
    })
}

function writeUpperCaseFile(data, filePath, callback) {

    fs.writeFile(path.join(filePath, ''), data, 'utf-8', (err) => {
        if (err) {
            console.log(err.message)
        }
        else {
            console.log(`${filePath} File Created  Succesfully`)
            callback(filePath)
        }
    })
}

function storeData(data, filePath, callback) {
    fs.writeFile(path.join(filePath, ''), data + '\n', 'utf-8', (err) => {
        if (err) {
            console.log(err.message)
        }
        else {
            console.log(`File Stored Successfully`)
            callback(data)
        }
    })
}

function appendFilePath(data, filePath, callback) {
    fs.appendFile(path.join(filePath, ''), data + '\n', 'utf-8', (err) => {
        if (err) {
            console.log(err.message)
        }
        else {
            console.log(`File Stored Successfully`)
            callback(data)
        }
    })
}

// Delete file in Directory

function deleteFiles(filePath) {
    fs.unlink(path.join(filePath, ''), (err) => {
        if (err) {
            console.log(err.message)
        }
        else {
            console.log('File Deleted')
            
        }
    })
};

module.exports = { readFile, writeUpperCaseFile, storeData, appendFilePath, deleteFiles };