const { createDir, crtFile, deleteFiles } = require('../problem1')

createDir('jsonFiles', () => {
    crtFile('./jsonFiles/file1.json', () => {
        crtFile('./jsonFiles/file2.json', () => {
            crtFile('./jsonFiles/file3.json', () => {
                crtFile('./jsonFiles/file4.json', () => {
                    crtFile('./jsonFiles/file5.json', () => {
                        deleteFiles('./jsonFiles/file1.json', () => {
                            deleteFiles('./jsonFiles/file2.json', () => {
                                deleteFiles('./jsonFiles/file3.json', () => {

                                })
                            })
                        })
                    })
                })
            })
        })
    })
})