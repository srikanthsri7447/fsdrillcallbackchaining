const { readFile, writeUpperCaseFile, storeData, appendFilePath, deleteFiles } = require('../problem2')


readFile('lipsum.txt', (data) => {
    writeUpperCaseFile(data.toUpperCase(), 'upperCaseFile.txt', (data) => {
        storeData(data, './fileNames.txt', (filePath) => {
            readFile(filePath, (data) => {
                let sentence = ''
                const lowerData = data.toLowerCase().trim().split('.')
                lowerData.map((eachSentc) => {
                    sentence += eachSentc.trim() + "\n"
                })
                writeUpperCaseFile(sentence, 'lowerCaseSentenceFile.txt', (filePath) => {
                    appendFilePath(filePath, './fileNames.txt', (filePath) => {
                        readFile(filePath, (data) => {
                            storeData(data, 'combinedData.txt', () => {
                                readFile('upperCaseFile.txt', (data) => {
                                    appendFilePath(data, 'combinedData.txt', () => {
                                        readFile('combinedData.txt', (data) => {
                                            const sortedData = data.toString().trim().split("\n").sort()

                                            let sentence = ''

                                            sortedData.map((eachSent) => {
                                                if (eachSent !== '') {
                                                    sentence += eachSent + "\n"
                                                }
                                            })
                                            storeData(sentence, 'combinedData.txt', () => {
                                                appendFilePath('combinedData.txt', './fileNames.txt', () => {
                                                    readFile('./fileNames.txt', (data) => {
                                                        const filePathArray = data.trim().split("\n")
                                                        filePathArray.map((eachPath) => {
                                                            deleteFiles(eachPath)
                                                        })
                                                    })
                                                })
                                            })
                                        })


                                    })

                                })

                            })
                        })

                    })

                })
            })

        })

    })
})

